<?php

use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');


/**
 * EJERCICIO 1 // RUTAS
 */

//1. Crea una ruta que tenga un parámetro que sea opcional y comprueba que funciona.

Route::get('/users/{name?}', function(){
    return view('users');
});

//2. Crea una ruta que tenga un parámetro que sea opcional y tenga un valor por defecto en caso de que no se especifique.
Route::get('products/{name?}', function($name = 'MyProduct'){
    return view('products', ['name' => $name]);
});

//3. Crea una ruta que atienda por POST y compruébala con Postman.

Route::post('fakepost', function(){
    return "Todo bien!";
});

//4. Crea una ruta que atienda por GET y por POST (en un único método) y compruébalas.

Route::match(['get', 'post'], '/admin', function(){
    return "Esta es la tierra de los admin";
});

//5. Crea una ruta que compruebe que un parámetro está formado sólo por números.

Route::get('posts/{id}', function($id){

    return "Este es el post $id";

})->where('id', '[0-9]+');

//6. Crea una ruta con dos parámetros que compruebe que el primero está formado sólo por letras y el segundo sólo por números.

Route::get('admin/{name}/messages/{id}', function($name, $id){

    return "Estamos viendo el mensaje $id del admin ".ucfirst($name);

})->where(['name' => '[A-Za-z]+','id' => '[0-9]+']);


/**
 * EJERCICIO 2 // HELPERS
 */


Route::get('host', function(){

    return "La dirección de esta BBDD es: ".env('DB_HOST');

});

Route::get('timezone', function(){
    return "Nuestra zona horaria es: ".config('app.timezone');
});

/**
 * EJERCICIO 3 // VISTAS
 */

//1

Route::view('/', 'home');

//2

/* Route::get('fecha', function(){

    return view('fecha', ['nameOfDay' => date('D'), 'date' => date('d'), 'month' => date('M'), 'year' => date('Y')]);
}); */

//3

/* Route::get('fecha', function(){
    $nameOfDay = date('D');
    $date = date('d');
    $month = date('M');
    $year = date('Y');
    return view('fecha', compact('nameOfDay', 'date', 'month', 'year'));
}); */

//4 

Route::get('fecha', function(){

    return view('fecha')
    ->with('nameOfDay', date('D'))
    ->with('date', date('d'))
    ->with('month', date('M'))
    ->with('year', date('Y'));

});




require __DIR__.'/auth.php';

